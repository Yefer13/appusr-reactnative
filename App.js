import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
//import
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'


//add function
  const Stack = createStackNavigator();

//import component
import UsersList from './screen/UsersList'
import CreateUsers from './screen/CreateUsers'
import UsersDetail from './screen/UsersDetail'

function MyStack(){
  return (
    <Stack.Navigator>
      <Stack.Screen name="UserList"  component={UsersList} options={{title:'Users List'}}/>
      <Stack.Screen name="CreateUsers"  component={CreateUsers} options={{title:'Create a new User'}}/>
      <Stack.Screen name="UsersDetail"  component={UsersDetail} options={{title:'User Detail'}}/>
    </Stack.Navigator>
  )
}  

export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
