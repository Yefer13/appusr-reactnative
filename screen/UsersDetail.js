import React, {useEffect, useState} from 'react'
import { View, StyleSheet, ScrollView,Button, ActivityIndicator, Alert } from 'react-native'
import firebase from '../database/firebase'
import { Input} from 'react-native-elements';

const UsersDetail = (props) => {

    const initialState = {
        id:'',
        name:'',
        email:'',
        phone:'',
    }

        const [user, setUser] = useState(initialState);
        
        const [loading, setLoading] = useState(true);

    const getUserById = async (id) =>{
        const dbRef = firebase.db.collection('users').doc(id)
        const doc = await dbRef.get();
        const user = doc.data();
        setUser({
            ...user,
            id: doc.id,
        });
        setLoading(false)
    }



    useEffect(() => {
        getUserById(props.route.params.userId)
    }, []);


    const handleChangeText = (name, value) => {
        setUser({ ...user, [name]: value })
    };

    const deleteUser = async () => {
        const dbRedf = firebase.db.collection('users').doc(props.route.params.userId);
        await dbRedf.delete();
        props.navigation.navigate('UserList')
    }

    const updateUser = async () => {
        const dbRef = firebase.db.collection('users').doc(user.id);
        await dbRef.set({
            name: user.name,
            email: user.email,
            phone: user.phone
        })
        setUser(initialState)
        props.navigation.navigate('UserList')
    }
    const openConfirmationsAlert = () =>{
        Alert.alert('Remove The User', 'Are you sure?', [
            {text: 'Yes', onPress: () => deleteUser()},
            {text: 'No', onPress: () => console.log(false)}
        ])
    }

    if(loading){
        return(
            <View>
                <ActivityIndicator size="large" color="#9e9e9e" />
            </View>
        )
    }


    return (        
        <ScrollView style={styles.container}>

            <View>
                <Input placeholder="Name User" 
                    value={user.name}
                    onChangeText={(value) => handleChangeText('name', value)}
                />
            </View>

            <View>
                <Input placeholder="Email User"
                    value={user.email}
                    onChangeText={(value) => handleChangeText('email', value)}
                />
            </View>

            <View>
                <Input placeholder="Phone User" 
                    value={user.phone}
                    onChangeText={(value) => handleChangeText('phone', value)}
                />
            </View>
            <View style = {styles.espacio}>
            <Button title="Update User" 
                color="#19AC52"
                onPress = {() => updateUser()} />
            </View>

            <View style = {styles.espacio}>
            <Button title="Delete User" 
                color="#E37399"
                onPress = {() => openConfirmationsAlert()} />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding: 35,
    },
    espacio:{
        marginBottom:18
    }
})

export default UsersDetail
